import React from "react";
import PropTypes from "prop-types";
import '../App.css';

const Forma = (props) => {
  let input;
  const { onSubmit } = props;
  return (
    <span className="App">
      <h1>Github username:</h1>
      <form
        onSubmit={(e) => {
          e.preventDefault();
          if (input.value !== "") {
            onSubmit(input.value);
          }
        }}
      >
        <input
          type="text"
          placeholder="Look for github user..."
          ref={(node) => {
            input = node;
          }}
        />
        <input type="submit" value="Go!" />
      </form>
    </span>
  );
};

Forma.propTypes = {
  onSubmit: PropTypes.func.isRequired,
};

export default Forma;
