import React from "react";
import PropTypes from "prop-types";
import '../App.css';

import Repos from "./compRepos";

const User = (props) => {
  const { currentUserData, userRepos } = props;
  const { userData } = currentUserData;
  const { repos, isFetching } = userRepos;
  console.log(currentUserData);
  return (
    <div>
      <h1>User details: {userData.login}</h1>
      <img
        alt={userData.login}
        style={{ width: "150px", height: "150px" }}
        src={userData.avatar_url}
      />
      <ul>
        <li>Name: {userData.name}</li>
        <li>Location: {userData.location}</li>
        <li>Bio: {userData.bio}</li>
      </ul>

      {!currentUserData.isFetching &&
        !userRepos.isFetching &&
        repos.length === 0 && (
          <h2> No repos found for user {userData.login}</h2>
        )}
      {!isFetching && repos.length > 0 && (
        <div>
          <h2>Repositories for {userData.name}</h2>
          <Repos repos={repos} />
        </div>
      )}
    </div>
  );
};

// definirati propTypes
User.propTypes = {
  currentUserData: PropTypes.object.isRequired,
};

export default User;
