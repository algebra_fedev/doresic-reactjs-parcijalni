import React from "react";

// ovo je (barem jedna) komponenta definirana  klasom
export default class Repos extends React.Component {
    constructor(props) {
        super(props);
      };

    render() {
        return (
            <ul>
            {this.props.repos.map((repo) => (
              <li key={repo.id}>
                <h3>{repo.name}</h3>
                <p>Desc: {repo.description}</p>
                <p>Language: {repo.language}</p>
                <hr></hr>
              </li>
            ))}
          </ul>
        );
    }

}
