import React from "react";
import { Provider } from "react-redux";
import configureStore from "./components/compStore";
import Start from "./components/Start";

const store = configureStore();

const App = () => (
  <Provider store={store}>
    <Start />
  </Provider>
);

export default App;
